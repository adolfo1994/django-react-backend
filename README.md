## Template Usage
This is a starter project for a Django backend according to the Fincite standards. It includes the following:

* Environment Variables (https://github.com/joke2k/django-environ)
* Logging Configured
* Email Configuration
* Django REST preinstalled 
* Coverage for test coverage
* Configuration to serve a separate Frontend (https://bitbucket.org/fincite/seed-frontend)
* Fabutils (for deployments) and Log-viewer (log files in the admin)
* Documentation generation (Sphinx) and viewing (Both Backend and Frontend in the admin)
* Generic README

To use it for your own project, clone this repository and replace project_name where you find it. Then update your git configuration to point to your new repository.

# project_name


## Installation
There is the complete list of packages required by the project (Ubuntu)
```
$ sudo apt-get install build-essential libssl-dev libffi-dev python-dev python python-virtualenv python-pip python3-venv libfreetype6 libfreetype6-dev pkg-config npm postgresql-client postgresql postgresql-contrib postgresql-server-dev-9.x git python3-dev
```

## Configure packages

### Postgres

```
$ sudo su postgres -c psql

postgres$ create user <user> with password '<password>';

postgres$ create database <database> owner <user> encoding 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
```

#### Only for local purposes to use fabric

```
postgres$ alter user <user> with superuser;
```

Edit the `/etc/postgresql/9.x/main/pg_hba.conf` file as root user:

```
local all postgres trust

local all all trust
```

Restart the service

```
$ /etc/init.d/postgresql restart
```

## Configure the project
### Create a virtualenv

```
$ python3 -m venv project_name
```

This command will create a new folder with the name `project_name`

### Clone the project

First verify your SSH Keys on bitbucket configuration `https://bitbucket.org/account/user/<your_user>/ssh-keys/`
then if you dont have a key that points to your computer follow this tutorials:

* https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html
* https://confluence.atlassian.com/bitbucket/add-an-ssh-key-to-an-account-302811853.html

```
$ git clone git@bitbucket.org:inkalabsinc/project_name.git
```

### Activate your enviroment
Inside the `project_name` folder run the following command

```
$ source bin/activate
```

After this you will see the virtualenv name in your prompt. i.e.:

```
(project_name) $
```

### Install requirements
```
(project_name)$ cd project_name-backend

(project_name)$ pip install -r requirements.txt
```

### Setting up environment variables for project

For environment variables configuration, you will need a .env file in the parent directory of the current folder.

```
(project_name) $ touch ../.env
```

An example configuration and how to obtain values for your virtualenv can be found in:
https://redmine.fincite.net/projects/management/wiki/Environment_variable_django_settings#Example

### Run the project

Once you have everything ok, you can run the project.

```
(project_name) $ ./manage.py check

(project_name) $ ./manage.py migrate

(project_name) $ ./manage.py runserver
```

### Run tests

Coverage is configured for the project for running tests and measuring in Scrutinizer

```
(project_name) $ coverage run --source="." manage.py test --settings=project_name.settings --verbosity=2
```

Once ran, if you want to see fast the results you can run

```
(project_name) $ coverage report
```

or you can run 

```
(project_name) $ coverage html
```

and an HTML view of your test coverage will be generated in htmlcov/index.html

### Build documentation files

Sphinx is configured to build a user friendly site for code documentation.

To build this files run

```
(project_name) $ cd docs
(project_name) $ make html
```

They will be build in docs/build/html/ with index.html as the main page.
It can also be accessed from the admin site in the top navigation.

# Dockerization

## Build process

To build your project using Docker:

```
$ docker-compose build --build-arg SSH_KEY="$(cat ~/.ssh/id_rsa)" --build-arg SSH_KEY_PUB="$(cat ~/.ssh/id_rsa.pub)" project
```
> NOTE:
> - Make sure your RSA keys are not configured with a password, otherwise build will fail. 
> 
> - New RSA public key must be added to your Bitbucket configuration to allow clone of private repositories in *requirements.txt* 

## Running the project

To start your project you can execute:

```
$ docker-compose up -d
```

When executing
```
$ docker ps
```

there should be at least 2 containers (for the database and the project itself). If you have included celery and memcached, 5 containers must be displayed (`memcached`, `db`, `celery`, `rabbitmq` and `projectsource`)


To shut down containers:
```
$ docker-compose down
```

> NOTE:
> 
> After configuring everything, the project should be running in localhost:8000, so be careful with port conflicts.

## Extra considerations

The file `docker-compose.yml` list the services that Docker will run, so feel free to comment / uncomment those that you need.