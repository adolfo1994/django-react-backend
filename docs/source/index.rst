.. project_name's documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to project_name's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

..
   Here add your other files.
   For each django app, create a folder in the apps folder with an index.rst.
   For each file in your django app, create a file in the app folder
   for example:
    apps/accounts/index will be the root file for the accounts django app
   it will be left as an example in the apps directory but feel free to change it

   apps/accounts/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
