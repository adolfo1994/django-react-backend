..
   Example index file for a django application

Accounts
========

Application for handling user accounts and groups.

Contents:

..  toctree::
    :maxdepth: 2

    models

..
    Here you will put the .rst files for each file in your Django app.
    the models.rst file is left as an example.
    