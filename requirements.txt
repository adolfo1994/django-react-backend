Django==2.1.2
django-filter==1.1.0
djangorestframework==3.8.2
Markdown==2.6.10
pytz==2018.5
django-environ==0.4.5
psycopg2-binary==2.7.5
django-dynamic-fixture==2.0.0
django-extensions==2.1.3
ipython==7.0.1
Sphinx==1.7.5
coverage==4.5.1

# Fabutils
-e git+git@bitbucket.org:fincite/fabutils.git@0.1.3.1#egg=fabutils

# Log Viewer
-e git+git@bitbucket.org:fincite/django-log-viewer.git@v0.0.4#egg=django_log_viewer
