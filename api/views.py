from django.shortcuts import render
from rest_framework import viewsets, serializers 
from rest_framework.permissions import AllowAny
from rest_framework.decorators import permission_classes

from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

@permission_classes((AllowAny, ))
class UsersViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    
